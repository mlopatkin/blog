/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.blog.phaser;

import java.util.concurrent.TimeUnit;

public class Main {

    private static Iterable<String> makeSource() {
        return new SlowReader(200000, 500, 0);
    }

    public static void main(String[] args) throws Exception {
        Config config = Config.parseConfig(args);

        ParallelIterableFactory factory = config.getTestFactory();
        try (TaskExecutor executor = new TaskExecutor(config.getConcurrencyLevel())) {
            if (config.isVerbose()) {
                System.err.printf(
                        "Running %s @%d threads\n", factory, config.getConcurrencyLevel());
            }
            // JIT warm-up
            for (int i = 0; i < config.getWarmupCount(); i++) {
                executor.execute(factory.makeParallelWrapper(makeSource()));
            }
            for (int i = 0; i < config.getTotalIterations(); ++i) {
                long executionTimeMs =
                        executor.execute(factory.makeParallelWrapper(makeSource())).elapsed(
                                TimeUnit.MILLISECONDS);
                System.out.println(executionTimeMs);
            }
        }

    }

}
