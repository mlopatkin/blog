/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.blog.phaser;

import com.google.common.collect.AbstractIterator;

import java.util.Iterator;
import java.util.Random;

public class SlowReader implements Iterable<String> {

    private final int mCount;

    private final int mSleepAfter;
    private final long mSleepDurationMs;

    private final Random mRandom = new Random();

    public SlowReader(int count, int sleepAfter, long sleepDurationMs) {
        mCount = count;
        mSleepAfter = sleepAfter;
        mSleepDurationMs = sleepDurationMs;
    }

    @Override
    public Iterator<String> iterator() {
        return new SlowIterator();
    }

    private class SlowIterator extends AbstractIterator<String> {

        private int mCurrent;

        @Override
        protected String computeNext() {
            if (mCurrent >= mCount) {
                return endOfData();
            }

            if (mCurrent % mSleepAfter == 0) {
                try {
                    Thread.sleep(mSleepDurationMs);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            ++mCurrent;
            return generateRandomString("0123456789abcdef", 16);
        }
    }

    private String generateRandomString(CharSequence alphabet, int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            final int rndPos = mRandom.nextInt(alphabet.length());
            sb.append(alphabet.charAt(rndPos));
        }
        return sb.toString();
    }
}
