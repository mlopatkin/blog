/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.blog.phaser;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

public class Config {

    @Parameter(names = {"-h", "--help"}, help = true)
    private boolean mHelp;

    @Parameter(names = {"-j"}, description = "Number of concurrent threads")
    private int mConcurrencyLevel = 1;

    @Parameter(names = {"-i"}, description = "Number of cycles")
    private int mTotalIterations = 10;

    @Parameter(names = {"-w", "--warmup-count"}, description = "Number of warm-up cycles (for JIT to compile)")
    private int mWarmupCount = 2;

    @Parameter(names = {"-t", "--test"}, description = "Test (single-item or buffered)")
    private String mClassUnderTest = "SINGLE_ITEM";

    @Parameter(names = {"-v", "--verbose"}, description = "More verbose output")
    private boolean mVerbose;

    private Config() {
    }

    public int getConcurrencyLevel() {
        return mConcurrencyLevel;
    }

    public int getTotalIterations() {
        return mTotalIterations;
    }

    public int getWarmupCount() {
        return mWarmupCount;
    }

    public boolean isVerbose() {
        return mVerbose;
    }

    public ParallelIterableFactory getTestFactory() {
        return ParallelIterableFactory.fromString(mClassUnderTest);
    }

    public static Config parseConfig(String[] args) {
        Config config = new Config();
        JCommander commander = new JCommander(config, args);
        if (config.mHelp) {
            commander.usage();
            System.exit(0);
        }
        try {
            config.getTestFactory();
        } catch (IllegalArgumentException e) {
            config.mClassUnderTest = "SINGLE_ITEM";
        }
        return config;
    }
}
