/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.blog.phaser;

import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

public class TaskExecutor implements AutoCloseable {

    private final int mConcurrencyLevel;
    private final ListeningExecutorService mExecutorService;

    public TaskExecutor(int concurrencyLevel) {
        mConcurrencyLevel = concurrencyLevel;
        mExecutorService =
                MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(mConcurrencyLevel));
    }

    public Stopwatch execute(Iterable<String> producer) {
        List<ListenableFuture<Integer>> futures = new ArrayList<>(mConcurrencyLevel);
        Stopwatch counter = Stopwatch.createStarted();
        for (int i = 0; i < mConcurrencyLevel; ++i) {
            futures.add(
                    mExecutorService.submit(new IterationTask(Utils.seq(producer.iterator()))));
        }

        Futures.getUnchecked(Futures.allAsList(futures));
        counter.stop();
        return counter;
    }

    @Override
    public void close() {
        mExecutorService.shutdown();
    }

    private static class IterationTask implements Callable<Integer> {

        private static final Pattern sLetters = Pattern.compile("[a-f]{16}");

        private final Iterable<String> mSource;
        private int mResult = 0;

        public IterationTask(Iterable<String> source) {
            mSource = source;
        }

        @Override
        public Integer call() {
            for (String s : mSource) {
                if (sLetters.matcher(s).matches()) {
                    ++mResult;
                }
            }
            return mResult;
        }
    }

}
