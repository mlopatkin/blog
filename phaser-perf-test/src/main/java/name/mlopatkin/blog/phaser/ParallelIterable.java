/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.blog.phaser;

import com.google.common.collect.AbstractIterator;

import java.util.Iterator;
import java.util.concurrent.Phaser;

public class ParallelIterable<T> implements Iterable<T> {

    private final Iterable<T> mSource;
    private final Iterator<T> mSourceIterator;

    private T mCurrent;
    private boolean mIterationCompleted;

    private final Phaser mPhaser = new Phaser() {
        @Override
        protected boolean onAdvance(int phase, int registeredParties) {
            mIterationCompleted = !mSourceIterator.hasNext();
            if (!mIterationCompleted) {
                mCurrent = mSourceIterator.next();
            }
            return super.onAdvance(phase, registeredParties);
        }
    };

    public ParallelIterable(Iterable<T> source) {
        mSource = source;
        mSourceIterator = mSource.iterator();
    }

    @Override
    public DiscardableIterator<T> iterator() {
        return new ParallelIterator();
    }

    private class ParallelIterator extends AbstractIterator<T> implements DiscardableIterator<T> {

        private boolean mClosed;

        ParallelIterator() {
            mPhaser.register();
        }

        @Override
        protected T computeNext() {
            if (mClosed) {
                throw new IllegalStateException("Iterator is closed");
            }
            mPhaser.arriveAndAwaitAdvance();
            if (mIterationCompleted) {
                close();
                return endOfData();
            } else {
                return mCurrent;
            }
        }

        @Override
        public void close() {
            if (!mClosed) {
                mPhaser.arriveAndDeregister();
                mClosed = true;
            }
        }
    }

    public interface DiscardableIterator<T> extends Iterator<T>, AutoCloseable {

        void close();
    }
}
