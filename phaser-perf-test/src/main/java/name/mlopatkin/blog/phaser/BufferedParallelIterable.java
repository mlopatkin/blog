/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.blog.phaser;

import com.google.common.collect.AbstractIterator;
import com.google.common.collect.Iterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Phaser;

public class BufferedParallelIterable<T> implements Iterable<T> {

    private final int mBufferSize;
    private final List<T> mBuffer;
    private final Iterator<T> mSourceIterator;

    private final Phaser mPhaser = new Phaser() {
        @Override
        protected boolean onAdvance(int phase, int registeredParties) {
            mBuffer.clear();
            if (mSourceIterator.hasNext()) {
                Iterators.addAll(mBuffer, Iterators.limit(mSourceIterator, mBufferSize));
            }
            return super.onAdvance(phase, registeredParties);
        }
    };

    public BufferedParallelIterable(int bufferSize, Iterable<T> source) {
        mBufferSize = bufferSize;
        mSourceIterator = source.iterator();
        mBuffer = new ArrayList<>(mBufferSize);
    }

    @Override
    public DiscardableIterator<T> iterator() {
        return new ParallelIterator();
    }

    private class ParallelIterator extends AbstractIterator<T> implements DiscardableIterator<T> {

        private boolean mClosed;
        private Iterator<T> mBufferIterator;

        ParallelIterator() {
            mPhaser.register();
            mBufferIterator = mBuffer.iterator();
        }

        @Override
        protected T computeNext() {
            if (mClosed) {
                throw new IllegalStateException("Iterator is closed");
            }
            if (mBufferIterator.hasNext()) {
                return mBufferIterator.next();
            }
            mPhaser.arriveAndAwaitAdvance();
            if (mBuffer.isEmpty()) {
                close();
                return endOfData();
            }
            // guarantee to have something
            mBufferIterator = mBuffer.iterator();
            return mBufferIterator.next();
        }

        @Override
        public void close() {
            if (!mClosed) {
                mPhaser.arriveAndDeregister();
                mClosed = true;
            }
        }
    }

    public interface DiscardableIterator<T> extends Iterator<T>, AutoCloseable {

        void close();

    }
}
