@echo off
setlocal
set RUN_CMD=%~dp0\..\build\install\phaser-perf-test\bin\phaser-perf-test.bat
set CFG=-w 5 -t %1 -i 30
for /L %%i in (1,1,9) do (
echo %%i 
%RUN_CMD% %CFG% -j %%i | tee %1_0%%i.dat
)

for /L %%i in (10,1,12) do (
echo %%i 
%RUN_CMD% %CFG% -j %%i -t %1 | tee %1_%%i.dat
)
