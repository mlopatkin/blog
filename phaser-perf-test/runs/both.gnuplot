#set term svg
#set output 'both.svg'
set terminal png
set output 'both.png'
set border 2 front linetype -1 linewidth 1.000
set boxwidth 0.5 absolute
set style fill   solid 0.25 border lt -1
set style boxplot fraction 0.9
set logscale y
set xlabel 'Number of threads'
set ylabel 'Time, ms'
set style data boxplot 
plot 'both.dat' using 1:2 t 'single-item' with points, \
     'both.dat' using 1:3 t 'buffered' with points
