@echo off
set PYTHON=C:\Python27\python
set GNUPLOT=gnuplot

if not "%1" == "" goto %1
:run
run single-item
run buffered
:combine
combine single-item
combine buffered
%PYTHON% median.py single-item_*.dat buffered_*.dat
:plot
%GNUPLOT% -e "dataset='single-item'" boxplot.gnuplot
%GNUPLOT% -e "dataset='buffered'" boxplot.gnuplot
%GNUPLOT% both.gnuplot