import sys
import glob

from itertools import izip

def GetMedianOfFile(filename):
	with open(filename, 'r') as f:
		items = [int(x.strip()) for x in f.readlines()]
	items.sort()
	return items[len(items)/2]


medians = [[GetMedianOfFile(f) for f in glob.iglob(pattern)] for pattern in sys.argv[1:]]
	
for i, m1, m2 in izip(xrange(1, 100), medians[0], medians[1]):
	print i, m1, m2