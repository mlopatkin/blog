@echo off
echo # %1 > %1.dat

for /l %%i in (1,1,9) do (
	for /f %%s in (%1_0%%i.dat) do (
		echo %%s  %%i >> %1.dat
	)
)

for /l %%i in (10,1,12) do (
	for /f %%s in (%1_%%i.dat) do (
		echo %%s  %%i >> %1.dat
	)
)
