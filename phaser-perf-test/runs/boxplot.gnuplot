# set terminal canvas  solid butt size 600,400 fsize 10 lw 1 fontscale 1 name "boxplot_1" jsdir "."
# set output 'boxplot.1.js'
set terminal png
set output dataset.'.png'
unset key
set border 2 front linetype -1 linewidth 1.000
set boxwidth 0.5 absolute
set style fill   solid 0.25 border lt -1
set style boxplot fraction 0.9
set style data boxplot 
set xlabel 'Number of threads'
set ylabel 'Time, ms'
plot dataset.'.dat' using (1):1:(0):2
