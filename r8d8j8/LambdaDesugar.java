import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class LambdaDesugar extends Activity {
    private Button mButton;

    public void onCreate(Bundle savedInstanceState) {
        mButton.setOnClickListener((View v) -> {
            Toast.makeText(v.getContext(), "Hello!", Toast.LENGTH_LONG).show();
        });
    }
}
