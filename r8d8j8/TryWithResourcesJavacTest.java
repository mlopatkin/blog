interface SuperCloseable extends AutoCloseable {

}

public class TryWithResourcesJavacTest implements AutoCloseable, SuperCloseable {
    public void tryConcreteType() throws Exception {
        try(TryWithResourcesJavacTest test = new TryWithResourcesJavacTest()) {
            System.out.println("tryConcreteType");
        }
    }


    public void tryAutocloseableType() throws Exception  {
        try(AutoCloseable test = new TryWithResourcesJavacTest()) {
            System.out.println("tryAutocloseableType");
        }
    }

    public void trySuperCloseable() throws Exception  {
        try(SuperCloseable test = new TryWithResourcesJavacTest()) {
            System.out.println("trySupercloseableType");
        }
    }

    @Override
    public void close() {
        System.out.println("Closed");
    }
}
