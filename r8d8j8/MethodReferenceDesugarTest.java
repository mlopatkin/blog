public class MethodReferenceDesugarTest {
    public static class StaticMethod {
        public static void staticRun() {
            System.out.println("staticRun");
        }
    }

    public static class InstanceMethod {
        public void instanceRun() {
            System.out.println("instanceRun");
        }
    }

    public static void main(String[] args) {
        new Thread(StaticMethod::staticRun).start();
        InstanceMethod i = new InstanceMethod();
        new Thread(i::instanceRun).start();
    }
}
